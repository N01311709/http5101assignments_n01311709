﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Assignment1a_N01311709
{
    public class Client
    {
        private string clientFName;
        private string clientLName;
        private string clientEmail;
        private string clientPhone;

        public Client()
        {

        }

        public string ClientFName
        {
            get { return clientFName; }
            set { clientFName = value; }
        }

        public string ClientLName
        {
            get { return clientLName; }
            set { clientLName = value; }
        }

        public string ClientEmail
        {
            get { return clientEmail; }
            set { clientEmail = value; }
        }

        public string ClientPhone
        {
            get { return clientPhone; }
            set { clientPhone = value; }
        }

    }
}