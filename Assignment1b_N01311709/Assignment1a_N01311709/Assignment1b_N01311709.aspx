﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Assignment1b_N01311709.aspx.cs" Inherits="Assignment1a_N01311709.Assignment1a_N01311709" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Chateau de Hamasoor Delan (Assignment 1A for HTTP5101)</title>
</head>
<body>
    <form id="form1" runat="server">
        <div><h1>Hotel Reservations for <em>Chateau de Hamasoor Delan</em></h1>
            <p><em>Customer Service:</em> customer@chateauhamasoor.com</p>
            <p>Welcome. Please fill out the form below to book your reservation.</p>
            <asp:ValidationSummary ID="validationsummary" runat="server" />
            <h2>Personal Information</h2>
            <p><asp:Label runat="server" ID="clientfnameLabel" AssociatedControlID="clientfname" Text="First Name:"></asp:Label>
                <asp:TextBox runat="server" ID="clientfname" placeholder="First Name"></asp:TextBox>
               <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter your first name." ControlToValidate="clientfname" ID="validatorfname"></asp:RequiredFieldValidator>
           <asp:Label runat="server" ID="clientlnameLabel" AssociatedControlID="clientlname" Text="Last Name:"></asp:Label>
                <asp:TextBox runat="server" ID="clientlname" placeholder="Last Name"></asp:TextBox>
                <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter your last name." ControlToValidate="clientlname" ID="validatorlname"></asp:RequiredFieldValidator>
            </p> 
            <p><asp:Label runat="server" ID="clientemailLabel" AssociatedControlID="clientemail" Text="Email"></asp:Label>
                <asp:TextBox runat="server" ID="clientemail" placeholder="Email Address"></asp:TextBox>
                <!-- Validation expression from the servercontrols example in HTTP5101 lecture, dated September 14, 2018 -->
                <asp:RegularExpressionValidator runat="server" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="clientemail" ID="validatoremail" ErrorMessage="Please enter a valid email address."></asp:RegularExpressionValidator>
             
                
                <asp:Label runat="server" ID="clientphoneLabel" AssociatedControlID="clientphone" Text="Phone Number"></asp:Label>
                <asp:TextBox runat="server" ID="clientphone" placeholder="Phone Number"></asp:TextBox>
                <!-- Validation expression from https://stackoverflow.com/questions/16699007/regular-expression-to-match-standard-10-digit-phone-number -->
                <asp:RegularExpressionValidator ID="clientphonevalidator" runat="server" ValidationExpression="^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$" ControlToValidate="clientphone" ErrorMessage="Please enter a valid phone number."></asp:RegularExpressionValidator>
          
            </p>
            <h2>Reservation Information</h2>
            <p><asp:Label runat="server" ID="checkindateLabel" AssociatedControlID="checkindate" Text="Check-In Date:"></asp:Label></p>
            <p><asp:Calendar runat="server" ID="checkindate"></asp:Calendar>
            <p><asp:Label runat="server" ID="checkoutdateLabel" AssociatedControlID="checkoutdate" Text="Check-Out Date"></asp:Label></p>
            <p><asp:Calendar runat="server" ID="checkoutdate"></asp:Calendar>
            </p>

            <p><asp:Label runat="server" ID="numAdultsLabel" AssociatedControlID="numAdults" Text="Number of Adults:"></asp:Label>
            <asp:TextBox runat="server" ID="numAdults" TextMode="Number" placeholder="Number of Adults (1-20 per booking)"></asp:TextBox>
            <asp:RangeValidator runat="server" ControlToValidate="numAdults" Type="Integer" MinimumValue="1" MaximumValue="20" ErrorMessage="There must be at least one adult. If your party includes more than 20 adults, please email customer service."></asp:RangeValidator>
            </p>

            <p><asp:Label runat="server" ID="numChildrenLabel" AssociatedControlID="numChildren" Text="Number of Children:"></asp:Label>
                <asp:TextBox runat="server" ID="numChildren" TextMode="Number" placeholder="Number of Children"></asp:TextBox></p>
            <asp:RangeValidator runat="server" ControlToValidate="numChildren" Type="Integer" MinimumValue="0" MaximumValue="20" ErrorMessage="If your party includes more than 20 children, please email customer service."></asp:RangeValidator>
            </p>

            <p><asp:Label runat="server" ID="clientpetsLabel" Text="Pets:"></asp:Label>
                <asp:RadioButtonList runat="server" ID="clientpets">
            <asp:ListItem Text="Yes">Yes</asp:ListItem>
            <asp:ListItem Text="No">No</asp:ListItem>
                </asp:RadioButtonList>
                <asp:RequiredFieldValidator runat="server" ErrorMessage="Tell us if you have pets." ControlToValidate="clientpets" ID="clientpetsvalidator"></asp:RequiredFieldValidator>
            </p>
           
            <p><asp:Label runat="server" ID="bedtypeLabel" Text="Bed Type:"></asp:Label>
                <asp:RadioButtonList runat="server" ID="bedType">
                <asp:ListItem Text="1 Queen Size">1 Queen Size</asp:ListItem>
                <asp:ListItem Text="2 Queen Size">2 Queen Size</asp:ListItem>
                <asp:ListItem Text="1 King Size">1 King Size</asp:ListItem>
                    </asp:RadioButtonList>
                <asp:RequiredFieldValidator runat="server" ErrorMessage="Please specify your bed type." ControlToValidate="bedType" ID="bedTypevalidator"></asp:RequiredFieldValidator>
            </p>
            <p>
            
            <asp:Label runat="server" ID="roomThemeLabel" AssociatedControlID="roomTheme" Text="Select your room theme."></asp:Label>
            <asp:DropDownList runat="server" ID="roomTheme">
                <asp:ListItem Value="R" Text="Regular"></asp:ListItem>
                <asp:ListItem Value="HO" Text="Homer's Odyssey"></asp:ListItem>
                <asp:ListItem Value="GP" Text="Grecian Balcony"></asp:ListItem>
                <asp:ListItem Value="JC" Text="Jungle Champion"></asp:ListItem>
                <asp:ListItem Value="5D" Text="'50's Diner"></asp:ListItem>
                <asp:ListItem Value="PT" Text="Chant d'extase dans un paysage triste"></asp:ListItem>
            </asp:DropDownList>
                <asp:RequiredFieldValidator runat="server" ErrorMessage="Please specify your room theme." ControlToValidate="roomTheme" ID="roomthemevalidator"></asp:RequiredFieldValidator>
             </p>
                <h2>Room Service</h2>
            <p><asp:Label runat="server" ID="roomserviceLabel" Text="What snacks do you want in your room?"></asp:Label>
                <asp:CheckBoxList runat="server" ID="roomService">
                <asp:ListItem Value="ALM" Text="Almonds" />
                <asp:ListItem Value="PIS" Text="Pistachios"/>
                <asp:ListItem Value="CHO" Text="Chocolate"/>
                <asp:ListItem Value="PRE" Text="Pretzels" />
                    </asp:CheckBoxList>
            </p>

                <h2>Let us know where you heard about us! (optional)</h2>
            <p><asp:Label runat="server" ID="referralLabel" Text="Where have you heard about us? (check all that apply)"></asp:Label></p>
            <p><asp:CheckBoxList runat="server" ID="referralType">
                <asp:ListItem Value="NET" Text="Internet ad" />
                <asp:ListItem Value="PRN" Text="Print media" />
                <asp:ListItem Value="FRN" Text="Friend" />
                <asp:ListItem Value="SOC" Text="Social media" />
               </asp:CheckBoxList>
            </p>
             <asp:Button runat="server" ID="reserveButton" OnClick="Reserve" Text="Reserve"/>
        </div>

        <div runat="server" id="reserveConfirm">

        </div>
    </form>
</body>
</html>
