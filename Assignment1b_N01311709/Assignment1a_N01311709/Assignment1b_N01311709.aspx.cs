﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Assignment1a_N01311709
{
    public partial class Assignment1a_N01311709 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Reserve(object sender, EventArgs e)
        {
            if (!Page.IsValid)
            {
                return;
            }

            // create list of snacks for room
            // the structure for this loop is taken from lines 129-139 of the servercontrols example in HTTP5101 lecture, dated September 28, 2018 -->

            List<string> snacks = new List<string>();

            foreach (Control control in roomService.Controls)
            {
                if (control.GetType() == typeof(CheckBox))
                {
                    CheckBox item = (CheckBox)control;
                    if (item.Checked)
                    {
                        snacks.Add(item.Text);
                    }
                }
            }

            // Reservation
            DateTime arrival = checkindate.SelectedDate;
            DateTime departure = checkoutdate.SelectedDate;
            //DateTime arrival = DateTime.ParseExact(checkindate.SelectedDate,"dd-MM-yyyy", CultureInfo.InvariantCulture);
            //DateTime departure = checkoutdate.SelectedDate.ParseExact("dd-MM-yyyy", CultureInfo.InvariantCulture);
            int adults = int.Parse(numAdults.Text);
            int children = int.Parse(numChildren.Text);
            string beds = bedType.Text.ToString();
            string themeofroom = roomTheme.Text.ToString();
            Reservation newreservation = new Reservation(arrival, departure, adults, children, beds, themeofroom, snacks);

            newreservation.service = newreservation.service.Concat(snacks).ToList();

            // Client

            string fname = clientfname.Text.ToString();
            string lname = clientlname.Text.ToString();
            string email = clientemail.Text.ToString();
            string phone = clientphone.Text.ToString();
            Client newclient = new Client();
            newclient.ClientFName = fname;
            newclient.ClientLName = lname;
            newclient.ClientEmail = email;
            newclient.ClientPhone = phone;


            Reserve yourreservation = new Reserve(newclient, newreservation);

            reserveConfirm.InnerHtml = yourreservation.PrintConfirmation();
        }
    }
}