﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Assignment1a_N01311709
{
    public class Reservation
    {

        public DateTime checkin;
        public DateTime checkout;
        public int adultnum;
        public int childnum;
        public string bed;
        public string theme;
        public List<string> service;
        
        public Reservation(DateTime da, DateTime db, int a, int c, string b, string th, List<string> serv)
        {
            checkin = da;
            checkout = db;
            adultnum = a;
            childnum = c;
            bed = b;
            theme = th;
            service = serv;

        }
    }

}