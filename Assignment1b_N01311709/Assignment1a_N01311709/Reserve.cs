﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Assignment1a_N01311709
{
    public class Reserve
    {
        public Client client;
        public Reservation reservation;

        public Reserve(Client c, Reservation res)
        {
            client = c;
            reservation = res;
        }

        public string PrintConfirmation()
        {
            string confirmation = "Booking Confirmation:<br>";
            confirmation += "Your total is: " + CalculatePrice().ToString() + "<br/>";
            confirmation += "Name: "+client.ClientFName+" "+client.ClientLName+"<br/>";
            confirmation += "Email: " + client.ClientEmail + "<br/>";
            confirmation += "Phone: " + client.ClientPhone + "<br/>";
            confirmation += "From " + reservation.checkin + " to " + reservation.checkout + "<br/>";
            confirmation += reservation.adultnum + " adults and " + reservation.childnum + " children.<br/>";
            confirmation += "You will be in the " + reservation.theme + " with " + reservation.bed + "<br/>";
            return confirmation;
        }

        public double CalculatePrice()
        {
            double total = 0;
            if (reservation.theme=="Regular")
            {
                total = 200;
            } else if(reservation.theme=="Homer's Odyssey")
            {
                total = 1000;
            } else if(reservation.theme=="Grecian Balcony")
            {
                total = 670;
            } else if(reservation.theme=="Jungle Champion")
            {
                total = 550;
            } else if(reservation.theme=="'50's Diner")
            {
                total = 50;
            } else if(reservation.theme== "Chant d'extase dans un paysage triste")
            {
                total = 420;
            }

            return total;
        }
    }
}